const express = require('express')
const jwt = require('jsonwebtoken')

const app = express()

app.get('/api', (req, res) => {
    res.json(
        {
            message: 'welcome to the api'
        }
    )
})

app.post('/api/posts', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authdata) => {
        if (err) {
            res.sendStatus(403)
        }
        else {
            res.json({
                message: 'posts created...',
                authdata
            });
        }
    })

})

app.post('/api/login', (req, res) => {
    //mock user
    const user = {
        id: 1,
        username: 'yashlin',
        email: 'yashlinn@telesure.co.za'
    }
    //sign token 
    jwt.sign({ user: user }, 'secretkey', (err, token) => {
        res.json({
            token: token
        });
    });
})

//verify token
function verifyToken(req, res, next) {
    //get the auth header value
    const bearerHeader = req.headers['authorization']

    if (typeof bearerHeader !== 'undefined') {
        const bearer = bearerHeader.split(' ')
        const token = bearer[1]
        req.token = token;
        next();
    }
    else {
        res.sendStatus(403)
    }
}

app.listen(5000, () => console.log('server started on 5000'))